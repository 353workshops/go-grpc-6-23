package unter

import (
	"fmt"
	"time"
)

type Kind byte

const (
	Regular Kind = iota + 1
	Pool
)

func (k Kind) String() string {
	switch k {
	case Regular:
		return "regular"
	case Pool:
		return "pool"
	}

	return fmt.Sprintf("<Kind %d>", k)
}

type Ride struct {
	ID        string
	Kind      Kind
	DriverID  string
	StartTime time.Time
	EndTime   time.Time
	Distance  float64 // in miles
}
