# Go gRPC

Miki Tebeka
<i class="far fa-envelope"></i> [miki@353solutions.com](mailto:miki@353solutions.com), <i class="fab fa-twitter"></i> [@tebeka](https://twitter.com/tebeka), <i class="fab fa-linkedin-in"></i> [mikitebeka](https://www.linkedin.com/in/mikitebeka/), <i class="fab fa-blogger-b"></i> [blog](https://www.ardanlabs.com/blog/)

#### Shameless Plugs

- [Go Essential Training](https://www.linkedin.com/learning/go-essential-training/) - LinkedIn Learning
    - [Rest of classes](https://www.linkedin.com/learning/instructors/miki-tebeka)
- [Go Brain Teasers](https://gumroad.com/l/Qkmou) book

---

## Links

- [mkcert](https://github.com/FiloSottile/mkcert) - Create local certificates
- [cue](https://cuelang.org/) - Validation
- [Test Containers](https://golang.testcontainers.org/quickstart/) - Testing
- [Falsehoods programmers believe about time](https://infiniteundo.com/post/25326999628/falsehoods-programmers-believe-about-time)
- [Falsehoods programmers believe about time zones](https://www.zainrizvi.io/blog/falsehoods-programmers-believe-about-time-zones/)
- [buf](https://buf.build/) - A protobuf/gRPC build tool
- [Writing gRPC interceptors in Go](https://shijuvar.medium.com/writing-grpc-interceptors-in-go-bf3e7671fe48)
- [Getting Peer Information](https://pkg.go.dev/google.golang.org/grpc/peer)
- [Go gRPC Middleware](https://github.com/grpc-ecosystem/go-grpc-middleware)
- [gRPC metadata](https://github.com/grpc/grpc-go/blob/master/Documentation/grpc-metadata.md)
- [gRPC status codes](https://grpc.github.io/grpc/core/md_doc_statuscodes.html)
- [Functional Options for Friendly APIs](https://dave.cheney.net/2014/10/17/functional-options-for-friendly-apis) by Dave Cheney
- [gRPCurl](https://github.com/fullstorydev/grpcurl)
    - [More gRPC tools](https://github.com/grpc-ecosystem/awesome-grpc#tools)
- [Go protobuf docs](https://pkg.go.dev/google.golang.org/protobuf)
    - [Types](https://protobuf.dev/programming-guides/proto3/#scalar)
- [gRPC in Go](https://grpc.io/docs/languages/go/quickstart/)

## Data & Other

- [start.json](_extra/start.json)
- [end.json](_extra/end.json)
- [Terminal log](_extra/terminal.log)

```
grpcurl --plaintext localhost:7771 list
grpcurl --plaintext localhost:7771 list Unter
grpcurl --plaintext localhost:7771 describe Unter
grpcurl --plaintext localhost:7771 describe StartRequest
grpcurl --plaintext -d @ localhost:7771  Unter.Start < start.json
```

## Setting Up

- You should know how to program in Go and have some experience with the command line
- You will need to have the following installed on your machine:
    - [The Go SDK](https://go.dev/dl/)
    -  An IDE such as [VSCode](https://code.visualstudio.com/) (with the [Go extension](https://marketplace.visualstudio.com/items?itemName=golang.Go) or [GoLand](https://www.jetbrains.com/go/)
    - `git`
    - The `protoc` compiler. You can install it from you package mangers (e.g. `brew`, `apt`, `choco`) or [download it](https://github.com/protocolbuffers/protobuf/releases)
