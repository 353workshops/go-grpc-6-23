func add(a, b int) int { ... }

# Regular function call
val = add(1, 2)

# RPC
data = marshal(a, b) // JSON, protobuf ...
send_to_server(data, "/add")

name, a, b = unmarshal(data)
fn = route(name)
val = fn(a, b)
data = marshal(val)
send_to_client(data)

val = unmarshal(data)

# Why RPC?
- Different implementation languages
- Scale
- Security
- Updates

RPC = medium/transport + encoding/serialization
gRPC = HTTP/2 + protocol buffers