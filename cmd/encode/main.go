package main

import (
	"fmt"
	"log"
	"os"

	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/timestamppb"

	"github.com/353solutions/unter/pb"
)

func main() {
	r := pb.StartRequest{
		Id:       "BS-8888",
		Kind:     pb.Kind_POOL,
		DriverId: "senna",
		Time:     timestamppb.Now(),
	}

	// fmt.Println(&r)
	data, err := proto.Marshal(&r)
	if err != nil {
		log.Fatalf("error: can't marshal - %s", err)
	}
	fmt.Printf("proto size: %d\n", len(data))
	// os.Stdout.Write(data)

	var r2 pb.StartRequest
	if err := proto.Unmarshal(data, &r2); err != nil {
		log.Fatalf("error: can't unmarshal - %s", err)
	}
	fmt.Println(&r2)

	// jdata, err := json.Marshal(&r)
	jdata, err := protojson.Marshal(&r)
	if err != nil {
		log.Fatalf("error: can't marshal JSONN - %s", err)
	}
	fmt.Printf("json size: %d\n", len(jdata))
	os.Stdout.Write(jdata)

}
