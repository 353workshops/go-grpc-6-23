package main

import (
	"context"
	"net"
	"testing"

	"github.com/353solutions/unter/pb"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func TestStart(t *testing.T) {
	s := Server{}
	req := pb.StartRequest{
		Id:       uuid.NewString(),
		Kind:     pb.Kind_POOL,
		DriverId: "007",
		Time:     timestamppb.Now(),
	}
	// without interceptors
	resp, err := s.Start(context.Background(), &req)
	require.NoError(t, err)
	require.Equal(t, req.Id, resp.Id)
}

// Option 2: Start server in goroutine, use client to call it

// Finding random free port
func freePort(t *testing.T) int {
	conn, err := net.Listen("tcp", "")
	if err != nil {
		t.Fatalf("free port: %s", err)
	}

	conn.Close()
	return conn.Addr().(*net.TCPAddr).Port
}
