package main

import (
	"context"
	"io"
	"log"
	"net"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/peer"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"

	"github.com/353solutions/unter/pb"
)

type Server struct {
	pb.UnimplementedUnterServer // required
}

func (s *Server) Start(ctx context.Context, req *pb.StartRequest) (*pb.StartResponse, error) {
	log.Printf("info: start request: %#v", req)
	if req.Id == "" || req.DriverId == "" || req.Time == nil {
		log.Printf("error: invalid request: %#v", req)
		return nil, status.Errorf(codes.InvalidArgument, "missing fields")
	}

	resp := pb.StartResponse{Id: req.Id}
	return &resp, nil
}

func (s *Server) End(ctx context.Context, req *pb.EndRequest) (*pb.EndResponse, error) {
	resp := pb.EndResponse{
		Id: req.Id,
	}
	return &resp, nil
}

func (s *Server) Update(stream pb.Unter_UpdateServer) error {
	var resp pb.UpdateResponse

	for {
		req, err := stream.Recv()
		if err == io.EOF {
			break
		}

		if err != nil {
			log.Printf("error: recv - %s", err)
			return err
		}

		log.Printf("info: update got %v", req)
		resp.Count++
	}

	if err := stream.SendAndClose(&resp); err != nil {
		log.Printf("error: close - %s", err)
	}
	return nil
}

func timingInterceptor(ctx context.Context, req any, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	start := time.Now()
	defer func() {
		duration := time.Since(start)
		log.Printf("info: metric: %s took %v", info.FullMethod, duration)
	}()

	p, ok := peer.FromContext(ctx)
	if ok {
		log.Printf("info: %s called from %s", info.FullMethod, p.Addr.String())
	}

	return handler(ctx, req)
}

func main() {
	addr := ":7771"
	log.Printf("info: server starting on %s", addr)
	lis, err := net.Listen("tcp", addr)
	if err != nil {
		log.Printf("error: can't listen - %s", err)
	}
	defer lis.Close()
	creds := insecure.NewCredentials()
	srv := grpc.NewServer(grpc.Creds(creds), grpc.UnaryInterceptor(timingInterceptor))
	var s Server
	pb.RegisterUnterServer(srv, &s)
	reflection.Register(srv)

	if err := srv.Serve(lis); err != nil {
		log.Fatalf("error: can't run - %s", err)
	}
}
