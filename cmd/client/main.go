package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/google/uuid"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/protobuf/types/known/timestamppb"

	"github.com/353solutions/unter/pb"
)

func main() {
	const addr = "localhost:7771"
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	creds := insecure.NewCredentials()
	// WithBlock will fail here if server is not up
	conn, err := grpc.DialContext(ctx, addr, grpc.WithTransportCredentials(creds), grpc.WithBlock())

	if err != nil {
		log.Fatalf("error: can't connect to %s - %s", addr, err)
	}
	log.Printf("connected to rides server at %s", addr)

	c := pb.NewUnterClient(conn)

	req := pb.StartRequest{
		Id:       uuid.NewString(),
		Kind:     pb.Kind_POOL,
		DriverId: "007",
		Time:     timestamppb.Now(),
	}

	ctx, cancel = context.WithTimeout(context.Background(), 30*time.Millisecond)
	defer cancel()

	resp, err := c.Start(ctx, &req)
	if err != nil {
		log.Fatalf("error: can't call Start - %s", err)
	}
	fmt.Printf("info: %s started\n", resp.Id)

	if err := update(c, "007", 7); err != nil {
		log.Fatalf("error: can't update - %s", err)
	}
}

func update(c pb.UnterClient, id string, count int) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stream, err := c.Update(ctx)
	if err != nil {
		return err
	}

	req := pb.UpdateRequest{
		DriverId: id,
		Location: &pb.Location{
			Lat: 51.4871871,
			Lng: -0.1270605,
		},
	}

	for i := 0; i < count; i++ {
		req.Location.Lat += 0.001
		req.Location.Lng += 0.001
		if err := stream.Send(&req); err != nil {
			return err
		}
		time.Sleep(200 * time.Millisecond) // simulate work
	}

	out, err := stream.CloseAndRecv()
	if err != nil {
		return err
	}

	log.Printf("info: %d out of %d updates\n", out.Count, count)
	return nil
}
